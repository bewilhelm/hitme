package de.hedgehogcode.hitme.entities;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class HitMeButton extends TextButton {

    private HitMeButtonMaster master;
    private Boolean okay;
    private float timeToShow;
    private float timeOver = 0;

    public HitMeButton(TextButtonStyle style, Boolean okay, float timeToShow, float x, float y, float width, float height, HitMeButtonMaster master) {
        super("", style);
        this.master = master;
        this.timeToShow = timeToShow;
        this.okay = okay;
        if (this.okay)
            setText("OK");
        else
            setText("NO");
        setX(x);
        setY(y);
        setWidth(width);
        setHeight(height);
        super.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                buttonClick();
            }
        });
    }

    public Boolean update(float delta) {
        timeOver += delta;
        //Gdx.app.log(HitMeGame.LOG, "Time Over: " + timeOver);
        if (timeOver > timeToShow)
            return false;
        else
            return true;
    }

    public Boolean isOkay() {
        return okay;
    }

    private void buttonClick() {
        master.buttonClick(this);
    }

    public interface HitMeButtonMaster {
        public abstract void buttonClick(HitMeButton button);
    }

    public HitMeButton setNewValues(boolean okay, float timeToShow, float x, float y, float width, float height) {
        this.okay = okay;
        if (this.okay)
            setText("OK");
        else
            setText("NO");
        this.timeToShow = timeToShow;
        this.timeOver = 0;
        setX(x);
        setY(y);
        setWidth(width);
        setHeight(height);
        return this;
    }
}
