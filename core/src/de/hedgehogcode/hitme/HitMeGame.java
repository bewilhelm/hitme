package de.hedgehogcode.hitme;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.I18NBundle;
import de.hedgehogcode.hitme.screens.StartScreen;

public class HitMeGame extends Game {

    public static final String VERSION = "1.1.0";
    public static final String LOG = "Hit Me";

    public static I18NBundle strings;
    public static SpriteBatch batch;

    @Override
    public void create() {
        if (batch == null) {
            batch = new SpriteBatch();
        }
        if (strings == null) {
            strings = I18NBundle.createBundle(Gdx.files.internal("strings/strings"));
        }
        setScreen(new StartScreen(this));
    }
}
