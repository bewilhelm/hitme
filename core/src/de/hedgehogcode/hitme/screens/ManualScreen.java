package de.hedgehogcode.hitme.screens;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import de.hedgehogcode.hitme.HitMeGame;

public class ManualScreen extends NormalScreen {

    private int countNo;

    public ManualScreen(HitMeGame game) {
        super(game);
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);

        float scale = width / 750f;
        middleTextLabel.setFontScale(scale);
        middleTextLabel.setX(0);
        middleTextLabel.setY(height / 2
                - middleTextLabel.getPrefHeight() / 2);
        middleTextLabel.setAlignment(Align.center);

        newGameButton.getLabel().setFontScale(1);
        newGameButton.setWidth(100);
        newGameButton.setHeight(60);
        newGameButton.setX(width / 3f - newGameButton.getWidth() / 2);
        newGameButton.setY(height * 1f / 6f - newGameButton.getHeight() / 2f);
        stage.addActor(newGameButton);

        secondButton.getLabel().setFontScale(1);
        secondButton.setWidth(100);
        secondButton.setHeight(60);
        secondButton.setX(width * 2f / 3f - secondButton.getWidth() / 2);
        secondButton.setY(height * 1f / 6f - secondButton.getHeight() / 2f);
        stage.addActor(secondButton);
    }

    @Override
    public void show() {
        super.show();

        headlineLabel.setText(HitMeGame.strings.get("manualScreenHeadline"));
        middleTextLabel.setText(HitMeGame.strings.get("manual0"));
        newGameButton.setText("OK");
        secondButton.setText("NO");

        newGameButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new StartScreen(game));
            }
        });
        countNo = 1;
        secondButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                String text = "";
                if (countNo < 16) {
                    text = HitMeGame.strings.get("manual" + countNo);
                } else if (countNo == 16) {
                    text = HitMeGame.strings.get("manual" + countNo);
                    secondButton.setText("OK");
                } else {
                    game.setScreen(new StartScreen(game));
                }
                middleTextLabel.setText(text);
                countNo++;
            }
        });
    }

}
