package de.hedgehogcode.hitme.screens;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;

import de.hedgehogcode.hitme.HitMeGame;
import de.hedgehogcode.hitme.entities.HitMeButton;
import de.hedgehogcode.hitme.entities.HitMeButton.HitMeButtonMaster;

public class PlayScreen implements Screen, HitMeButtonMaster {

    private static final int START_LIVES = 5;
    private static final float TIME_BETWEEN_BUTTONS = 3.0f;
    private static final float MIN_TIME_BUTTON = 1.0f;
    private static final float MAX_TIME_BUTTON = 2.0f;
    private static final String LIVES_TEXT = "<";
    private static final int BACKGROUND_IMG_WIDTH = 1024;
    private static final int BACKGROUND_IMG_HEIGHT = 1024;

    private float timeSinceLastButton;
    private int lives;
    private int points;

    private HitMeGame game;
    private ArrayList<HitMeButton> activeHitMeButtonList;
    private ArrayList<HitMeButton> unactiveHitMeButtonList;
    private Label livesLabel;
    private Label pointsLabel;
    private TextButtonStyle style;
    private Stage stage;

    private Texture backgroundTexture;
    private Sprite[][] backgroundSprite;

    public PlayScreen(HitMeGame game) {
        this.game = game;
    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0.98f, 0.98f, 0.98f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Button hinzufuegen
        timeSinceLastButton += delta;
        if (Math.random() * TIME_BETWEEN_BUTTONS * 5 < timeSinceLastButton) {
            HitMeButton button;
            float timeToShow = (float) ((Math.random() * (MAX_TIME_BUTTON - MIN_TIME_BUTTON)) + MIN_TIME_BUTTON);
            float width;
            float height;
            float x;
            float y;
            boolean okay = Math.random() > 0.5;

            boolean collision;
            do {
                width = 100;
                height = 60;
                x = (float) (Math.random() * (Gdx.graphics.getWidth() - width));
                y = (float) (Math.random() * (Gdx.graphics.getHeight() - height - pointsLabel.getHeight()));
                collision = false;
                for (HitMeButton colButton : activeHitMeButtonList) {
                    if ((colButton.getX() < x + width && x < colButton.getX() + colButton.getWidth()) &&
                            (colButton.getY() < y + height && y < colButton.getY() + colButton.getHeight()))
                        collision = true;
                }
            } while (collision);


            if (unactiveHitMeButtonList.size() == 0)
                button = new HitMeButton(style, okay, timeToShow, x, y, width, height, this);
            else {
                button = unactiveHitMeButtonList.get(0).setNewValues(okay, timeToShow, x, y, width, height);
                unactiveHitMeButtonList.remove(0);
            }
            stage.addActor(button);
            activeHitMeButtonList.add(button);
            timeSinceLastButton = 0;
        }
        // Entfernen der abgelaufenen Buttons
        for (int i = 0; i < activeHitMeButtonList.size(); i++) {
            HitMeButton button = activeHitMeButtonList.get(i);
            if (!button.update(delta)) {
                button.remove();
                activeHitMeButtonList.remove(button);
                unactiveHitMeButtonList.add(button);
                if (button.isOkay())
                    decLives();
                i--;
            }
        }

        stage.act(delta);

        HitMeGame.batch.begin();
        for (Sprite[] bgList : backgroundSprite)
            for (Sprite bg : bgList)
                bg.draw(HitMeGame.batch);
        HitMeGame.batch.end();

        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        livesLabel.setX(width * 1f / 50f);
        livesLabel.setY(height * 49f / 50f - livesLabel.getPrefHeight());
        stage.addActor(livesLabel);

        pointsLabel.setX(width * 49f / 50f - pointsLabel.getPrefWidth());
        pointsLabel.setY(height * 49f / 50f - pointsLabel.getPrefHeight());
        stage.addActor(pointsLabel);
        //Gdx.app.log(HitMeGame.LOG, "width: " + pointsLabel.getWidth());

        int countSpritesX = (1 + width / BACKGROUND_IMG_WIDTH + 1);
        int countSpritesY = (1 + height / BACKGROUND_IMG_HEIGHT + 1);
        backgroundSprite = new Sprite[countSpritesX][countSpritesY];
        for (int i = 0; i < countSpritesX; i++) {
            for (int j = 0; j < countSpritesY; j++) {
                backgroundSprite[i][j] = new Sprite(backgroundTexture);
                backgroundSprite[i][j].setX(i * BACKGROUND_IMG_WIDTH);
                backgroundSprite[i][j].setY(j * BACKGROUND_IMG_HEIGHT);
            }
        }
    }

    @Override
    public void show() {

        TextureAtlas atlas = new TextureAtlas("buttons/buttons.pack");
        Skin skin = new Skin();
        skin.addRegions(atlas);
        BitmapFont font = new BitmapFont(Gdx.files.internal("fonts/font.fnt"));
        style = new TextButtonStyle();
        style.up = skin.getDrawable("normal");
        style.down = skin.getDrawable("pressed");
        style.font = font;


        lives = START_LIVES;
        points = 0;

        LabelStyle labelStyle = new LabelStyle();
        labelStyle.font = font;
        livesLabel = new Label(livesText(), labelStyle);
        pointsLabel = new Label(points + "", labelStyle);
        pointsLabel.setAlignment(0, 3);

        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
        activeHitMeButtonList = new ArrayList<HitMeButton>();
        unactiveHitMeButtonList = new ArrayList<HitMeButton>();
        timeSinceLastButton = 0;
        backgroundTexture = new Texture("img/Grunge-004.jpg");

        HitMeButton button = new HitMeButton(style, true, 2.0f, 30, 30, 100, 60, this);

        activeHitMeButtonList.add(button);

        stage.addActor(button);

    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub

    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

    @Override
    public void dispose() {
        game.dispose();
        stage.dispose();

    }

    @Override
    public void buttonClick(HitMeButton button) {
        button.remove();
        activeHitMeButtonList.remove(button);
        unactiveHitMeButtonList.add(button);
        if (button.isOkay())
            incPoints();
        else
            decLives();

    }

    private void decLives() {
        lives--;
        livesLabel.setText(livesText());
        if (lives == 0) {
            game.setScreen(new GameOverScreen(game, points));
        }
    }

    private void incPoints() {
        points++;
        pointsLabel.setText(points + "");
		/*
		float width = 19;
		for (int i = 10; i <= points; i *= 10)
			width += 19;
			*/
        pointsLabel.setX(Gdx.graphics.getWidth() - pointsLabel.getPrefWidth() * 1.1f);
    }

    private String livesText() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < lives; i++) {
            builder.append(LIVES_TEXT);
        }
        return builder.toString();
    }
}
