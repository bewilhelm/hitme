package de.hedgehogcode.hitme.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import de.hedgehogcode.hitme.HitMeGame;


public class LegalNoticeScreen extends NormalScreen {

    private Screen previousScreen;
    private Label label;
    private ScrollPane scrollPane;
    private Table table;
    private TextButton backButton;
    private TextButton adMobButton;
    private TextButton websiteButton;

    public LegalNoticeScreen(HitMeGame game, Screen previousScreen) {
        super(game);
        this.previousScreen = previousScreen;
    }

    @Override
    public void show() {
        super.show();
        headlineLabel.remove();
        middleTextLabel.remove();
        newGameButton.remove();
        secondButton.remove();

        BitmapFont font = new BitmapFont(Gdx.files.internal("fonts/font.fnt"));

        LabelStyle labelStyle = new LabelStyle();
        labelStyle.font = font;

        label = new Label("\n" + HitMeGame.strings.get("legalNotice") + "\n\n\n" + HitMeGame.strings.get("privacyInfo"), labelStyle);
        label.setAlignment(Align.center);

        table = new Table();

        scrollPane = new ScrollPane(table);

        TextureAtlas atlas = new TextureAtlas("buttons/buttons.pack");
        Skin buttonSkin = new Skin(atlas);
        TextButtonStyle textButtonStyle = new TextButtonStyle();
        textButtonStyle.up = buttonSkin.getDrawable("normal");
        textButtonStyle.down = buttonSkin.getDrawable("pressed");
        textButtonStyle.font = font;
        textButtonStyle.fontColor = Color.BLACK;

        backButton = new TextButton(HitMeGame.strings.get("back"), textButtonStyle);
        backButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(previousScreen);
            }
        });

        adMobButton = new TextButton(HitMeGame.strings.get("adMob"), textButtonStyle);
        adMobButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Gdx.net.openURI("http://de.admob.com/home/privacy");
            }
        });

        websiteButton = new TextButton(HitMeGame.strings.get("website"), textButtonStyle);
        websiteButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Gdx.net.openURI("http://hedgehogcode.de/hit-me/");
            }
        });

        table.center();

        table.add(label);
        table.row();
        table.add(adMobButton);
        table.row();
        table.add(websiteButton);

        stage.addActor(scrollPane);
        stage.addActor(backButton);
        Gdx.input.setInputProcessor(stage);

    }


    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        float fontScale = width / 800f;
        label.setFontScale(fontScale);
        scrollPane.setBounds(width * 1f / 100f, height * 1f / 10f, width * 49f / 50f, height * 9f / 10f);
        backButton.getLabel().setFontScale(fontScale);
        backButton.setBounds(width * 1f / 20f, height * 1f / 40f, width * 1f / 4f, height * 1f / 20f);
        adMobButton.getLabel().setFontScale(fontScale);
        websiteButton.getLabel().setFontScale(fontScale);

        headlineLabel.remove();
        middleTextLabel.remove();
        newGameButton.remove();
        secondButton.remove();
    }
}
