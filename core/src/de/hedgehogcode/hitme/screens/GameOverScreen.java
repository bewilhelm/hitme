package de.hedgehogcode.hitme.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import de.hedgehogcode.hitme.HitMeGame;

public class GameOverScreen extends NormalScreen {
    private int points;


    public GameOverScreen(HitMeGame game, int points) {
        super(game);
        this.points = points;
    }

    @Override
    public void show() {
        super.show();
        Preferences pref = Gdx.app.getPreferences("highscore");
        int bestScore = pref.getInteger("best", 0);
        if (bestScore < this.points) {
            bestScore = this.points;
            pref.putInteger("best", bestScore);
            pref.flush();
        }
        headlineLabel.setText(HitMeGame.strings.format("gameOverScreenHeadline"));
        middleTextLabel.setText(HitMeGame.strings.format("gameOverScreenMiddleText", this.points, bestScore));
        newGameButton.setText(HitMeGame.strings.get("gameOverScreenNewGameButton"));
        secondButton.setText(HitMeGame.strings.get("helpButton"));

        newGameButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new PlayScreen(game));
            }
        });

        secondButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new ManualScreen(game));
            }
        });

    }

}
