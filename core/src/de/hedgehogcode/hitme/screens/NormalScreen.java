package de.hedgehogcode.hitme.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.Align;

import de.hedgehogcode.hitme.HitMeGame;

public abstract class NormalScreen implements Screen {

    protected HitMeGame game;

    private static final int BACKGROUND_IMG_WIDTH = 1024;
    private static final int BACKGROUND_IMG_HEIGHT = 1024;

    protected Stage stage;
    protected Label headlineLabel;
    protected Label middleTextLabel;
    protected TextButton newGameButton;
    protected TextButton secondButton;
    private Texture backgroundTexture;
    private Sprite[][] backgroundSprite;
    private Sprite bwprogrammingSprite;

    public NormalScreen(HitMeGame game) {
        this.game = game;
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.98f, 0.98f, 0.98f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);

        if (Gdx.input.justTouched()
                && bwprogrammingSprite.getBoundingRectangle().contains(
                Gdx.input.getX(),
                Gdx.graphics.getHeight() - Gdx.input.getY())) {
            //Gdx.net.openURI("http://bwprogramming.jimdo.com/apps/hit-me/impressum/");
            game.setScreen(new LegalNoticeScreen(game, this));
        }

        HitMeGame.batch.begin();
        for (Sprite[] bgList : backgroundSprite)
            for (Sprite bg : bgList)
                bg.draw(HitMeGame.batch);
        bwprogrammingSprite.draw(HitMeGame.batch);
        HitMeGame.batch.end();

        stage.draw();

    }

    @Override
    public void resize(int width, int height) {
        float scale = width / 720f;
        headlineLabel.setFontScale(scale);
        headlineLabel.setX(width / 2 - headlineLabel.getPrefWidth() / 2);
        headlineLabel.setY(height * 5f / 6f
                - headlineLabel.getPrefHeight() / 2f);
        stage.addActor(headlineLabel);

        scale = width / 550f;
        middleTextLabel.setFontScale(scale);
        middleTextLabel.setWidth(width);
        middleTextLabel.setX(0);
        middleTextLabel.setY(height / 2
                - middleTextLabel.getPrefHeight() / 2);
        middleTextLabel.setAlignment(Align.center);
        stage.addActor(middleTextLabel);

        scale = width / 550f;
        newGameButton.getLabel().setFontScale(scale);
        newGameButton
                .setWidth(newGameButton.getLabel().getPrefWidth() * 1.2f);
        newGameButton
                .setHeight(newGameButton.getLabel().getPrefHeight() * 1.5f);
        newGameButton.setX(width / 2 - newGameButton.getWidth() / 2);
        newGameButton.setY(height * 1f / 6f - newGameButton.getHeight() / 2f);
        stage.addActor(newGameButton);

        scale = width / 800f;
        secondButton.getLabel().setFontScale(scale);
        secondButton
                .setWidth(secondButton.getLabel().getPrefWidth() * 1.2f);
        secondButton
                .setHeight(secondButton.getLabel().getPrefHeight() * 1.5f);
        secondButton.setX(width / 2 - secondButton.getWidth() / 2);
        secondButton.setY(height * 1f / 12f - secondButton.getHeight() / 2f);
        stage.addActor(secondButton);

        int countSpritesX = (1 + width / BACKGROUND_IMG_WIDTH + 1);
        int countSpritesY = (1 + height / BACKGROUND_IMG_HEIGHT + 1);
        backgroundSprite = new Sprite[countSpritesX][countSpritesY];
        for (int i = 0; i < countSpritesX; i++) {
            for (int j = 0; j < countSpritesY; j++) {
                backgroundSprite[i][j] = new Sprite(backgroundTexture);
                backgroundSprite[i][j].setX(i * BACKGROUND_IMG_WIDTH);
                backgroundSprite[i][j].setY(j * BACKGROUND_IMG_HEIGHT);
            }
        }


        float spriteScale = (width * 1f / 3f) / bwprogrammingSprite.getWidth();
        float spriteWidth = width * 1f / 3f;
        float spriteHeight = bwprogrammingSprite.getHeight() * spriteScale;
        bwprogrammingSprite.setBounds(width * 1f / 3f, height * 1f / 100f, spriteWidth, spriteHeight);
    }

    @Override
    public void show() {
        TextureAtlas atlas = new TextureAtlas("buttons/buttons.pack");
        Skin skin = new Skin();
        skin.addRegions(atlas);

        BitmapFont font = new BitmapFont(Gdx.files.internal("fonts/font.fnt"));
        BitmapFont fontH = new BitmapFont(Gdx.files.internal("fonts/fontH.fnt"));

        LabelStyle labelStyle = new LabelStyle();
        labelStyle.font = font;

        LabelStyle labelStyleH = new LabelStyle();
        labelStyleH.font = fontH;

        TextButtonStyle buttonStyle = new TextButtonStyle();
        buttonStyle.down = skin.getDrawable("pressed");
        buttonStyle.up = skin.getDrawable("normal");
        buttonStyle.font = font;

        headlineLabel = new Label("Override!", labelStyleH);
        middleTextLabel = new Label("Override!", labelStyle);
        newGameButton = new TextButton("Override!", buttonStyle);
        secondButton = new TextButton("Override!", buttonStyle);

        backgroundTexture = new Texture("img/Grunge-004.jpg");
        bwprogrammingSprite = new Sprite(new Texture("img/bwprogramming.png"));
        bwprogrammingSprite.setAlpha(0.5f);

        stage = new Stage();
        Gdx.input.setInputProcessor(stage);

    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub

    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub

    }

}
