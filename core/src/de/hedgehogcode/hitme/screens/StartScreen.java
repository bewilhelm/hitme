package de.hedgehogcode.hitme.screens;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import de.hedgehogcode.hitme.HitMeGame;

public class StartScreen extends NormalScreen {


    public StartScreen(HitMeGame game) {
        super(game);

    }

    @Override
    public void show() {
        super.show();


        headlineLabel.setText(HitMeGame.strings.get("game"));
        middleTextLabel.setText(HitMeGame.strings.get("startScreenMiddleText"));
        newGameButton.setText(HitMeGame.strings.get("startScreenStartButton"));
        secondButton.setText(HitMeGame.strings.get("helpButton"));

        newGameButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new PlayScreen(game));
            }
        });

        secondButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new ManualScreen(game));
            }
        });

    }
}
