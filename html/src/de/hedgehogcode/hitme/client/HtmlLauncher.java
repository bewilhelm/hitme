package de.hedgehogcode.hitme.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import de.hedgehogcode.hitme.HitMeGame;

public class HtmlLauncher extends GwtApplication {

    @Override
    public GwtApplicationConfiguration getConfig() {
        return new GwtApplicationConfiguration(500, 600);
    }

    @Override
    public ApplicationListener createApplicationListener() {
        return new HitMeGame();
    }
}