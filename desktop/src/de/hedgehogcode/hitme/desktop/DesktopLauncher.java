package de.hedgehogcode.hitme.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import de.hedgehogcode.hitme.HitMeGame;

public class DesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = 562;
        config.height = 1000;
        config.resizable = true;
        new LwjglApplication(new HitMeGame(), config);
    }
}
