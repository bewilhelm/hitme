# Hit Me

Hit Me is a fun little game for android.

It's available in the Play-Store: [Link](https://play.google.com/store/apps/details?id=de.bw.hitme.android)

## Getting started

### Prerequisites

* Gradle
* Android-SDK

### Running and Debugging

See [Running and Debugging a LibGDX project](https://libgdx.badlogicgames.com/documentation/gettingstarted/Running%20and%20Debugging.html)

## Build with

* [libGDX](https://libgdx.badlogicgames.com/)

## Contributing

This is a dead project and I won't spend any time on it e.g. solve issues, review pull-requests. Feel free to fork and do your own stuff with it.

## License

This project is licensed under the 2-Clause BSD License - see the [LICENSE](LICENSE) file for details.

## TODOs

* Create ios icons
